
var theNumber;
var guesses;
var howMany;

function reset() {
	
	theNumber = Math.floor(Math.random() * 100) + 1;
	console.log(theNumber);
	
	guesses = 0;
	
	showHighScore();
	 
	howManyGuesses();
	
	createNumpad();
	
}

function howManyGuesses() {
	howMany = "# of guesses : " + guesses;
	document.getElementById("guesses").innerHTML = howMany;
}

function showHighScore() {
	
	if (localStorage.score !== undefined) {
		document.getElementById("hs").innerHTML = "<p><i>" + localStorage.name + "</i> with <i>" + localStorage.score + "</i> guesses</p>";
	} else {
		document.getElementById("hs").innerHTML = "<p><i>Yourself!</i></p>";
	}
}

function guess() {
	
	guesses++;
	
	howManyGuesses()
	
	var numberGuessed = document.getElementById(this.id).id;
	numberGuessed++; // id on numero - 1, eli nappi 1 = id="0" jne.
	
	if (numberGuessed < theNumber) {
		for (var i = 0; i < numberGuessed; i++) {
			var k = document.getElementById(i);
			k.style.visibility = 'hidden';
			k.removeEventListener("click", guess);
		}
			
	} else if (numberGuessed > theNumber) { 
		if(numberGuessed == 100) {
				var k = document.getElementById(99);
				k.style.visibility = 'hidden';
				k.removeEventListener("click", guess);
			} else {
				for (var i = 99; i >= numberGuessed; i--) {					
					var k = document.getElementById(i);
					var l = document.getElementById(numberGuessed - 1);
					k.style.visibility = 'hidden';
					k.removeEventListener("click", guess);
					l.style.visibility = 'hidden';
					l.removeEventListener("click", guess);
				}
			}
			
	} else {
		
		var x = document.getElementsByTagName("td");
		var t = document.getElementById(numberGuessed - 1);	
		
		for (var i = 0; i < x.length; i++) {
			x[i].innerHTML = i + 1;
			x[i].id = i;
			x[i].removeEventListener("click", guess);			
			if (x[i] !== t) { 
				x[i].style.visibility = 'hidden';
			}
		}
		
		var name = document.getElementById("name").value;
		
		if (!localStorage.score) {
			if (!name) { 
				name = "Unknown";
			}
			localStorage.name = name;
			localStorage.score = guesses;
		}
		
		var reveal = theNumber + ", you got it!";		
					
		if (guesses < localStorage.score) {
			if (!name) {
				localStorage.name = "Unknown";
			} else {
				localStorage.name = name;
			}
						
			localStorage.score = guesses;
			
			reveal += "<br>...and the Highscore!";
		}
		
		document.getElementById("reveal").innerHTML = reveal;
		
		showHighScore();
	}
}

function createNumpad() {
	
	var c, r, t;
    t = document.createElement('table');
	for (var i = 0; i <= 9; i++) {
		r = t.insertRow(i);
		for (var j = 0; j <= 9; j++) {
			c = r.insertCell(0);	
		}	
	}
	
	document.getElementById("numpad").appendChild(t);
	
	var x = document.getElementsByTagName("td");
	
	for (var i = 0; i < x.length; i++) {
		x[i].innerHTML = i + 1;
		x[i].id = i; 
		x[i].addEventListener("click", guess);
	}   	
}

function clearStorage() {
	localStorage.clear();
	showHighScore();
}